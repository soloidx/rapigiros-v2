from django.views.generic import TemplateView

from common import views as common_views


class DashboardMainView(TemplateView, common_views.AuthenticatedViewMixin):
    """Fist view of user dashboard"""
    template_name = "dashboard_user_main.html"
