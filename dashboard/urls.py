# -*- coding:utf-8 -*-
#pylint: disable=E1120
from django.conf.urls import patterns
from . import views

urlpatterns = patterns('',
        (r'^$', views.DashboardMainView.as_view()),
)
