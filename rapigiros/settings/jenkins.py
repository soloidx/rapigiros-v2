from . import *

#adding the django debug toolbar
INSTALLED_APPS += (
    'django_jenkins',
)

PROJECT_APPS = ['common', 'login', 'offices',  'dashboard']
