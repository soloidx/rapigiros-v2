from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'rapigiros.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    url(r'^admin/', include(admin.site.urls)),

    url(r'^login/', include('login.urls')),
    url(r'^$', include('dashboard.urls')),
    url(r'^dashboard/', include('dashboard.urls')),
    url(r'^giros/', include('money_transfer.urls')),
)
