from django import forms

from . import models


class SendMoneyForm(forms.ModelForm):
    """docstring for SendMoneyForm"""
    search_sender = forms.CharField(max_length=100)
    search_recipient = forms.CharField(max_length=100)

    def __init__(self, *args, **kwargs):
        super(SendMoneyForm, self).__init__(*args, **kwargs)
        self.fields.keyOrder = [
                'search_sender',
                'search_recipient',
                'client_sender',
                'client_recipient',
                'office_to',
                'amount',
                'rate',
                ]

    class Meta:
        model = models.MoneyTransfer
        widgets = {
                'client_sender': forms.HiddenInput,
                'client_recipient': forms.HiddenInput,
                }
