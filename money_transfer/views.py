from django.views.generic import FormView

from common import views as common_views
from . import forms

class SendMoneyView(FormView, common_views.AuthenticatedViewMixin):
    """Fist view of user dashboard"""
    template_name="send_money.html"
    form_class = forms.SendMoneyForm
    success_url = '/dashboard'

    def get_context_data(self, *args, **kwargs):
        context_data = super(SendMoneyView, self)\
                .get_context_data(*args, **kwargs)
        return context_data
