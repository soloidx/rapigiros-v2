# -*- coding:utf-8 -*-
#pylint: disable=E1120

from django.conf.urls import patterns
from . import views

urlpatterns = patterns('',
        (r'^enviar$', views.SendMoneyView.as_view()),
)
