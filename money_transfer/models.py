from django.db import models

from offices import models as offices_models

class Client(models.Model):
    """docstring for Client"""
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    search_field = models.TextField()

    def save(self, *args, **kwargs):
        self.search_field = "%s %s" % (self.first_name, self.last_name)
        super(AuditableModelMixin, self).save(*args, **kwargs)

    def search_client(self, query):
        pass

    def __str__(self):
        return "%s %s" % (self.first_name, self.last_name)


class MoneyTransfer(models.Model):
    """docstring for MoneyTransfer"""
    transfer_date = models.DateTimeField(auto_now_add=True, editable=False)
    office_form = models.ForeignKey(offices_models.Office,
            related_name='office_from',
            editable=False)
    office_to = models.ForeignKey(offices_models.Office,
            related_name='office_to', verbose_name="Oficina")
    client_sender = models.ForeignKey(Client, related_name='client_sender', 
            verbose_name="De")
    client_recipient = models.ForeignKey(Client,
            related_name='client_recipient', 
            verbose_name="Para")
    amount = models.DecimalField(max_digits=6, decimal_places=2, 
            verbose_name="Monto")
    rate = models.DecimalField(max_digits=6, decimal_places=2, 
            verbose_name="Comision")
