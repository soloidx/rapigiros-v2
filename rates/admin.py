from django.contrib import admin

from . import models

class RateDetailInline(admin.TabularInline):
    model = models.RateDetail


class Rate(admin.ModelAdmin):
    inlines = [
        RateDetailInline,
    ]


admin.site.register(models.AsignedRate)
admin.site.register(models.Rate, Rate)
