from django.db import models

from offices import models as offices_models

class Rate(models.Model):
    """docstring for Rate"""
    name = models.CharField(max_length=50, verbose_name="Nombre")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Comision"
        verbose_name_plural = "Comisiones"

class RateDetail(models.Model):
    """docstring for Rate"""
    amount = models.DecimalField(max_digits=6, decimal_places=2, \
            verbose_name="Monto")
    fee = models.DecimalField(default=0, max_digits=6, decimal_places=2, \
            verbose_name="Comision")
    percentage = models.DecimalField(default=0, max_digits=3, \
            decimal_places=2, verbose_name="Porcentaje")
    rate = models.ForeignKey(Rate)

    class Meta:
        verbose_name = "Detalle de comision"
        verbose_name_plural = "Detalles de comision"

class AsignedRate(models.Model):
    """docstring for AsignedRate"""
    office1 = models.ForeignKey(offices_models.Office, related_name='Office1')
    office2 = models.ForeignKey(offices_models.Office, related_name='Office2')
    rate = models.ForeignKey(Rate)

    def __str__(self):
        return 'De: %s a %s' % (self.office1, self.office2)
        
    class Meta:
        verbose_name = "Comision asignada"
        verbose_name_plural = "Comisiones Asignadas"
