from django.conf.urls import patterns

urlpatterns = patterns('',
        (r'^$', 'django.contrib.auth.views.login',
            {'template_name': 'login_login.html'}
        ),
        (r'^logout$', 'django.contrib.auth.views.logout_then_login'),
)
