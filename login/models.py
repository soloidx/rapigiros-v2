# -*- coding:utf-8 -*-

from django.db import models
from django.contrib.auth import models as auth_models

from offices import models as office_models

class User(auth_models.AbstractUser):
    """docstring for User"""
    office = models.ForeignKey(office_models.Office, null=True)
