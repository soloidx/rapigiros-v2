# -*- coding:utf-8 -*-

from django.db import models
from common import models as common_models

class State(models.Model, common_models.AuditableModelMixin):
    """Store the states (higher level)"""
    name = models.CharField(max_length=50, verbose_name="Nombre")

    def __str__(self):
        return self.name

    class Meta: # pylint: disable=C1001 
        verbose_name = "Departamento"


class Region(models.Model, common_models.AuditableModelMixin):
    """Store the Region (medium level)"""
    name = models.CharField(max_length=50, verbose_name="Nombre")
    state = models.ForeignKey(State)

    def __str__(self):
        return self.name

    class Meta: # pylint: disable=C1001
        verbose_name = "Provincia"


class District(models.Model, common_models.AuditableModelMixin):
    """Store the Districts (lower level)"""
    name = models.CharField(max_length=50, verbose_name="Nombre")
    region = models.ForeignKey(Region)

    def __str__(self):
        return self.name

    class Meta: # pylint: disable=C1001
        verbose_name = "Distrito"


class Office(models.Model):
    """Store the Offices"""
    name = models.CharField(max_length=50, verbose_name="Nombre")
    district = models.ForeignKey(District)

    def __str__(self):
        return "%s (%s)" % (self.name, self.district)

    class Meta: # pylint: disable=C1001
        verbose_name = "Oficina"
