# -*- coding:utf-8 -*-
#pylint: disable=R0904

from django.test import TestCase

from offices import models

class StateTestCase(TestCase):
    """State class tests"""

    def test_str_should_retur_name(self):
        test_string = "string"
        state = models.State()
        state.name = test_string
        self.assertEqual(str(state), test_string)


class RegionTestCase(TestCase):
    """State class tests"""

    def test_str_should_retur_name(self):
        test_string = "string"
        region = models.Region()
        region.name = test_string
        self.assertEqual(str(region), test_string)


class DistrictTestCase(TestCase):
    """District class tests"""

    def test_str_should_retur_name(self):
        test_string = "string"
        district = models.District()
        district.name = test_string
        self.assertEqual(str(district), test_string)


class OfficeTestCase(TestCase):
    """Office class tests"""

    def test_str_should_retur_name_plus_district(self):
        office_name = "office"
        district_name = "district"
        result_name = "office (district)"

        district = models.District()
        district.name = district_name
        office = models.Office()
        office.name = office_name
        office.district = district

        self.assertEqual(str(office), result_name)
