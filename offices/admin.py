from django.contrib import admin

from . import models

admin.site.register(models.State)
admin.site.register(models.Region)
admin.site.register(models.District)
admin.site.register(models.Office)
