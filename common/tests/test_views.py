# -*- coding:utf-8 -*-
#pylint: disable=R0904

from django.test import TestCase
from django.http import HttpResponseRedirect
from mock import Mock

from common import views

class AuthenticatedViewMixinTestCase(TestCase):
    """ base clase of auth tests"""

    def test_dispatch_sould_return_redirect_if_no_auth(self):
        mock_request = Mock()
        mock_request.user.is_authenticated = Mock(return_value=False)
        view = views.AuthenticatedViewMixin()
        response = view.dispatch(mock_request)
        assert isinstance(response, HttpResponseRedirect)

    def test_dispatch_sould_return_respose_if_auth(self):
        mock_request = Mock()
        mock_request.user.is_authenticated = Mock(return_value=True)
        mock_request.method = "GET"
        view = views.AuthenticatedViewMixin()
        view.request = mock_request
        response = view.dispatch(mock_request)
        assert not isinstance(response, HttpResponseRedirect)
