# -*- coding:utf-8 -*-

#import datetime

from django.db import models
from django.utils.translation import ugettext as _

#from cached_user.utils import get_non_anonymous_user

# Create your models here.

class AuditableModelMixin(object):
    """
    AuditableModelMixin helps to storage some useful fields for know which user
    are created edited or modified the data
    """
    created = models.DateTimeField(
            verbose_name=_('Created'),
            auto_now_add=True,
            editable=False
    )
    modified = models.DateTimeField(
            verbose_name=_('Modified'),
            auto_now_add=True,
            editable=False
    )

    #created_by = models.CharField(
            #max_length=50,
            #verbose_name=_('Created by'),
            #editable=False
    #)

    #modified_by = models.CharField(
            #max_length=50,
            #verbose_name=_('Created by'),
            #editable=False
    #)

    #def save(self, *args, **kwargs):
        #TODO: get the user chached
        #current_user = get_non_anonymous_user()

        #check if the is a new object
        #if not self.id:
            #self.modified_by = current_user.username

        #self.modified_by = current_user.username
        #super(AuditableModelMixin, self).save(*args, **kwargs)


    class Meta:
        abstract = True
