from django.shortcuts import redirect
from django.views.generic import View

class AuthenticatedViewMixin(View):
    """docstring for AuthenticatedView"""
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect('/login/?next=%s' % request.path)
        return super(AuthenticatedViewMixin, self)\
                .dispatch(request, *args, **kwargs)
